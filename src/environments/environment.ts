// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyD0jh7HmmvKriwq71vP3T5AzW4fMyQZw4A',
    authDomain: 'controle-ifsp-tee.firebaseapp.com',
    databaseURL: 'https://controle-ifsp-tee.firebaseio.com',
    projectId: 'controle-ifsp-tee',
    storageBucket: 'controle-ifsp-tee.appspot.com',
    messagingSenderId: '897840204580',
    appId: '1:897840204580:web:99e48f3194379246a5c1b1',
    measurementId: 'G-3VF7GZTFH6'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
