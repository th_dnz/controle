import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ForgotPageRoutingModule } from './forgot-routing.module';

import { ForgotPage } from './forgot.page';
import { TopoLoginComponent } from '../component/topo-login/topo-login.component';


@NgModule({
  imports: [    
    CommonModule,
    FormsModule,
    IonicModule,
    ForgotPageRoutingModule,
    ReactiveFormsModule,
    TopoLoginComponent
  ],
  declarations: [ForgotPage]
})
export class ForgotPageModule {}
