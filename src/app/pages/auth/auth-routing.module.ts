import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { CommonModule } from '@angular/common';
import { RegisterPage } from './register/register.page';
import { LoginPage } from './login/login.page';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ForgotPage } from './forgot/forgot.page';
import { TopoLoginComponent } from './component/topo-login/topo-login.component';

const routes: Routes = [
  {
      path: '', children: [
        {path: 'l', component: LoginPage },
        {path: 'forgot', component: ForgotPage },
        {path: 'register', component: RegisterPage },

      ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [
    LoginPage,
    ForgotPage,
    RegisterPage,
    TopoLoginComponent    
  ]
})
export class AuthRoutingModule { }
