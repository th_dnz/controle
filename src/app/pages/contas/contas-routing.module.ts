import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagarPage } from './pagar/pagar.page';
import { ReceberPage} from './receber/receber.page';
import { CadastroPage} from './cadastro/cadastro.page';
import { RelatorioPage} from './relatorio/relatorio.page';


const routes: Routes = [
  {
    path: '', children: [
      {path: 'pagar', component: PagarPage},
      {path: 'receber', component: ReceberPage},
      {path: 'relatorio', component: RelatorioPage},
      {path: 'cadastro', component: CadastroPage},
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ContasRoutingModule { }
